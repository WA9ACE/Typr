package de.ithinkinco.typr;

import java.util.ArrayList;
import java.util.List;

public class DocumentCollection {
	
	private List<Document> mDocumentList = new ArrayList<Document>();
	
	public DocumentCollection() {
	}
	
	public boolean add(Document document) {
		mDocumentList.add(document);
		return true;
	}
	
	public Document getDocumentAt(int index) {
		return mDocumentList.get(index);
	}
	
	public void removeDocumentAt(int index) {
		mDocumentList.remove(index);
	}
}
