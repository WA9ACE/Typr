package de.ithinkinco.typr;

import java.io.IOException;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

public class TypeWriter {
	
	private AudioInputStream mWav;
	private Clip mClip;
	
	public static final String ENTER		= "/audio/KeypressReturn.wav";
	public static final String SPACE		= "/audio/KeypressSpacebar.wav";
	public static final String BACKSPACE	= "/audio/KeypressDelete.wav";
	public static final String ANY_KEY		= "/audio/KeypressStandard.wav";
	
	public void playSound(String key) {
		try {
			mWav = AudioSystem.getAudioInputStream(this.getClass().getResourceAsStream(key));
			mClip = AudioSystem.getClip();
			mClip.open(mWav);
			mClip.setFramePosition(0);
			mClip.start();
		} catch (UnsupportedAudioFileException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (LineUnavailableException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
