package de.ithinkinco.typr;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JTabbedPane;

public class FileCloser implements ActionListener {

	private JTabbedPane mTabbedPane;
	private DocumentCollection mDocumentCollection;
	
	public FileCloser(JTabbedPane tabbedPane, DocumentCollection documentCollection) {
		mTabbedPane = tabbedPane;
		mDocumentCollection = documentCollection;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if(mTabbedPane.getTabCount() > 1) {
			mTabbedPane.removeTabAt(mTabbedPane.getSelectedIndex());
		}
	}
}
