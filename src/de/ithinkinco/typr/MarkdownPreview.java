package de.ithinkinco.typr;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JTabbedPane;

import com.petebevin.markdown.MarkdownProcessor;

public class MarkdownPreview implements ActionListener {

	private JFrame mFrame;
	private JTabbedPane mTabbedPane;
	private DocumentCollection mDocumentCollection;
	private JEditorPane mEditorPane = new JEditorPane();
	private MarkdownProcessor mProcessor = new MarkdownProcessor();
	private String html;
	
	public MarkdownPreview(JTabbedPane tabbedPane, DocumentCollection documentCollection) {
		mTabbedPane = tabbedPane;
		mDocumentCollection = documentCollection;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		String text = mDocumentCollection.getDocumentAt(mTabbedPane.getSelectedIndex()).getText();
		html = mProcessor.markdown(text);
		
		mFrame = new JFrame();
		mFrame.setBounds(100, 100, 640, 480);
		mFrame.setTitle("Markdown Preview");
		
		mEditorPane.setContentType("text/html");
		mEditorPane.setEditable(false);
		mEditorPane.setText(html);
		
		mFrame.getContentPane().add(mEditorPane, BorderLayout.CENTER);
		
		mFrame.setVisible(true);
		//mFrame.getContentPane().add(, BorderLayout.CENTER);
	}

}
