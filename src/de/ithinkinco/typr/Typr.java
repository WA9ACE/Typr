package de.ithinkinco.typr;

import java.awt.BorderLayout;
import java.awt.ComponentOrientation;
import java.awt.EventQueue;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JToolBar;
import javax.swing.KeyStroke;
import javax.swing.UIManager;

public class Typr {

	public JToolBar mToolBar = new JToolBar();
	public JTabbedPane mTabbedPane = new JTabbedPane(JTabbedPane.TOP);
	public DocumentCollection mDocumentCollection = new DocumentCollection();
	
	private JFrame mFrame;
	private FileBlank mFileBlank = new FileBlank(mTabbedPane, mDocumentCollection);
	private FileOpener mFileOpener = new FileOpener(mTabbedPane, mDocumentCollection);
	private FileCloser mFileCloser = new FileCloser(mTabbedPane, mDocumentCollection);
	private FileSaver mFileSaver = new FileSaver(mTabbedPane, mDocumentCollection);
	private MarkdownPreview mPreview = new MarkdownPreview(mTabbedPane, mDocumentCollection);

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		System.setProperty("com.apple.mrj.application.apple.menu.about.name", "Typr");
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					System.setProperty("apple.laf.useScreenMenuBar", "true");
					UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
					Typr window = new Typr();
					window.mFrame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Typr() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		mFrame = new JFrame();
		mFrame.setBounds(100, 100, 640, 480);
		mFrame.setTitle("Typr");
		mFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		KeyHandler keyHandler = new KeyHandler();
		mFrame.getContentPane().add(mTabbedPane, BorderLayout.CENTER);
		
		JScrollPane scrollPane = new JScrollPane();
		mTabbedPane.addTab("New Document", scrollPane);
		
		JTextArea textArea = new JTextArea();
		mDocumentCollection.add(new Document(textArea));
		textArea.addKeyListener(keyHandler);
		scrollPane.setViewportView(textArea);
		
		drawMenuBar();
		drawToolBar();
	}
	
	public void buildMenuBar() {
		
	}
	
	public void drawMenuBar() {
		JMenuBar menuBar = new JMenuBar();
		mFrame.setJMenuBar(menuBar);
		
		int platformDefaultKey = Toolkit.getDefaultToolkit().getMenuShortcutKeyMask();
		
		JMenu mnFile = new JMenu("File");
		menuBar.add(mnFile);
		
		JMenuItem mntmNew = new JMenuItem("New");
		mntmNew.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, platformDefaultKey));
		mntmNew.addActionListener(mFileBlank);
		mnFile.add(mntmNew);
		
		JMenuItem mntmOpen = new JMenuItem("Open");
		mntmOpen.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, platformDefaultKey));
		mntmOpen.addActionListener(mFileOpener);
		mnFile.add(mntmOpen);
		
		JMenuItem mntmSave = new JMenuItem("Save");
		mntmSave.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, platformDefaultKey));
		mntmSave.addActionListener(mFileSaver);
		mnFile.add(mntmSave);
		
		JMenuItem mntmSaveAs = new JMenuItem("Save As");
		mnFile.add(mntmSaveAs);
		
		JMenuItem mntmCloseFile = new JMenuItem("Close File");
		mntmCloseFile.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_W, platformDefaultKey));
		mntmCloseFile.addActionListener(mFileCloser);
		mnFile.add(mntmCloseFile);
		
		JMenuItem mntmExit = new JMenuItem("Exit");
		mntmExit.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Q, platformDefaultKey));
		mnFile.add(mntmExit);
		
		JMenu mnEdit = new JMenu("Edit");
		menuBar.add(mnEdit);
		
		JMenuItem mntmCut = new JMenuItem("Cut");
		mnEdit.add(mntmCut);
		
		JMenuItem mntmCopy = new JMenuItem("Copy");
		mnEdit.add(mntmCopy);
		
		JMenuItem mntmPaste = new JMenuItem("Paste");
		mnEdit.add(mntmPaste);
		
		JMenu mnTools = new JMenu("Tools");
		menuBar.add(mnTools);
		
		JMenuItem mntmWordCount = new JMenuItem("Word Count");
		mntmWordCount.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Document document = mDocumentCollection.getDocumentAt(mTabbedPane.getSelectedIndex());
				JOptionPane.showMessageDialog(mFrame, "Word Count: " + document.getWordCount());
			}
		});
		mnTools.add(mntmWordCount);
		
		JMenuItem mntmMarkdownPreview = new JMenuItem("Markdown Preview");
		mntmMarkdownPreview.addActionListener(mPreview);
		mnTools.add(mntmMarkdownPreview);
		
		JMenu mnHelp = new JMenu("Help");
		menuBar.add(mnHelp);
		
		JMenuItem mntmCheckUpdates = new JMenuItem("Check for Updates");
		mnHelp.add(mntmCheckUpdates);
		
		JMenuItem mntmAbout = new JMenuItem("About");
		mntmAbout.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F1, 0));
		mntmAbout.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(mFrame, "Typr\n A Markdown editor for Windows, Mac, and Linux.");
			}
		});
		mnHelp.add(mntmAbout);
		
		JButton btnX = new JButton("x");
		menuBar.add(Box.createHorizontalGlue());
		btnX.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
		btnX.addActionListener(mFileCloser);
		menuBar.add(btnX);
	}
		
	public void drawToolBar() {
		mFrame.getContentPane().add(mToolBar, BorderLayout.NORTH);
		
		JButton btnNew = new JButton();
		btnNew.setToolTipText("New");
		btnNew.setBorderPainted(false);
		btnNew.setOpaque(false);
		btnNew.setIcon(new ImageIcon(this.getClass().getResource("/toolbar-icons/new.png")));
		btnNew.addActionListener(mFileBlank);
		mToolBar.add(btnNew);
		
		JButton btnOpen = new JButton();
		btnOpen.setToolTipText("Open");
		btnOpen.setBorderPainted(false);
		btnOpen.setOpaque(false);
		btnOpen.setIcon(new ImageIcon(this.getClass().getResource("/toolbar-icons/folder.png")));
		btnOpen.addActionListener(mFileOpener);
		mToolBar.add(btnOpen);
		
		JButton btnSave = new JButton();
		btnSave.setToolTipText("Save");
		btnSave.setBorderPainted(false);
		btnSave.setOpaque(false);
		btnSave.setIcon(new ImageIcon(this.getClass().getResource("/toolbar-icons/disk.png")));
		btnSave.addActionListener(mFileSaver);
		mToolBar.add(btnSave);
		
		JButton btnPreview = new JButton();
		btnPreview.setToolTipText("Markdown Preview");
		btnPreview.setBorderPainted(false);
		btnPreview.setOpaque(false);
		btnPreview.setIcon(new ImageIcon(this.getClass().getResource("/toolbar-icons/monitor.png")));
		btnPreview.addActionListener(mPreview);
		mToolBar.add(btnPreview);
		
		mToolBar.add(Box.createHorizontalGlue());

		JButton btnSettings = new JButton();
		btnSettings.setToolTipText("Settings");
		btnSettings.setBorderPainted(false);
		btnSettings.setOpaque(false);
		btnSettings.setIcon(new ImageIcon(this.getClass().getResource("/toolbar-icons/gear.png")));
		mToolBar.add(btnSettings);
		
		JButton btnInfo = new JButton();
		btnInfo.setToolTipText("File Info");
		btnInfo.setBorderPainted(false);
		btnInfo.setOpaque(false);
		btnInfo.setIcon(new ImageIcon(this.getClass().getResource("/toolbar-icons/info.png")));
		mToolBar.add(btnInfo);
	}
}
