package de.ithinkinco.typr;

import java.io.BufferedWriter;
import java.io.IOException;

import javax.swing.JTextArea;

public class Document {
	
	private JTextArea mTextArea;
	
	public Document(JTextArea textArea) {
		mTextArea = textArea;
	}
	
	public String getText() {
		return mTextArea.getText();
	}
	
	public void writeDocument(BufferedWriter outFile) {
		try {
			mTextArea.write(outFile);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public int getWordCount() {
		String text = mTextArea.getText();
		String[] words = text.split(" ");
		return words.length;
	}
}
