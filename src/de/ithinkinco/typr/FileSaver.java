package de.ithinkinco.typr;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import javax.swing.JFileChooser;
import javax.swing.JTabbedPane;

public class FileSaver implements ActionListener {

	private JFileChooser mFileChooser = new JFileChooser();
	private JTabbedPane mTabbedPane;
	private DocumentCollection mDocumentCollection;
	
	public File mFile;
	public String mFileName;
	
	public FileSaver(JTabbedPane tabbedPane, DocumentCollection documentCollection) {
		mTabbedPane = tabbedPane;
		mDocumentCollection = documentCollection;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		int choice = mFileChooser.showSaveDialog(mTabbedPane);
		
		if(choice != JFileChooser.APPROVE_OPTION) return;
		mFile = mFileChooser.getSelectedFile();
		mFileName = mFile.getName();
		
		try {
			BufferedWriter outFile = new BufferedWriter(new FileWriter(mFile));
			mDocumentCollection.getDocumentAt(mTabbedPane.getSelectedIndex()).writeDocument(outFile);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

}
