package de.ithinkinco.typr;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class KeyHandler implements KeyListener {
	
	private final static int enter		= 10;
	private final static int space		= 32;
	private final static int backspace	= 8;
	
	private final TypeWriter mTypeWriter = new TypeWriter();

	@Override
	public void keyPressed(KeyEvent e) {
		switch(e.getKeyCode()) {
		case enter:
			mTypeWriter.playSound(TypeWriter.ENTER);
			break;
			
		case space:
			mTypeWriter.playSound(TypeWriter.SPACE);
			break;
			
		case backspace:
			mTypeWriter.playSound(TypeWriter.BACKSPACE);
			break;
			
		default:
			mTypeWriter.playSound(TypeWriter.ANY_KEY);
			break;
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
	}

}
