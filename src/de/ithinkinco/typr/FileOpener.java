package de.ithinkinco.typr;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import javax.swing.JFileChooser;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;

public class FileOpener implements ActionListener {
	
	private JFileChooser mFileChooser = new JFileChooser();
	private JTabbedPane mTabbedPane;
	private DocumentCollection mDocumentCollection;

	public File mFile;
	public String mFileName;
	
	public FileOpener(JTabbedPane tabbedPane, DocumentCollection documentCollection) {
		mTabbedPane = tabbedPane;
		mDocumentCollection = documentCollection;
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		int choice = mFileChooser.showOpenDialog(mTabbedPane);
		
		if(choice != JFileChooser.APPROVE_OPTION) return;
		mFile = mFileChooser.getSelectedFile();
		mFileName = mFile.getName();
		
		try {
			JScrollPane scrollPane = new JScrollPane();
			mTabbedPane.addTab(mFileName, scrollPane);
			
			JTextArea textArea = new JTextArea();
			mDocumentCollection.add(new Document(textArea));
			textArea.addKeyListener(new KeyHandler());
			scrollPane.setViewportView(textArea);
			
			BufferedReader br = new BufferedReader(new FileReader(mFile));
			String line = br.readLine();
			
			while(line != null) {
				textArea.append(line + "\n");
				line = br.readLine();
			}
			br.close();
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
