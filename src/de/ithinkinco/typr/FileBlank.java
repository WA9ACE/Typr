package de.ithinkinco.typr;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;

public class FileBlank implements ActionListener {
	
	private JTabbedPane mTabbedPane;
	private DocumentCollection mDocumentCollection;
	
	public FileBlank(JTabbedPane tabbedPane, DocumentCollection documentCollection) {
		mTabbedPane = tabbedPane;
		mDocumentCollection = documentCollection;
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		JScrollPane scrollPane = new JScrollPane();
		mTabbedPane.addTab("New Document", scrollPane);
		
		JTextArea textArea = new JTextArea();
		mDocumentCollection.add(new Document(textArea));
		scrollPane.setViewportView(textArea);
		mTabbedPane.addTab("New Document", scrollPane);
	}

}
